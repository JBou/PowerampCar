package net.patzleiner.powerampcar;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.media.MediaDescription;
import android.media.browse.MediaBrowser;
import android.text.TextUtils;

import com.maxmpz.poweramp.player.PowerampAPI;
import com.maxmpz.poweramp.player.TableDefs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Gabriel Patzleiner
 */
public class MediaItemsCollector {

    private final PowerampService service;

    public MediaItemsCollector(PowerampService service) {
        this.service = service;
    }

    // Media IDs used on browseable items of MediaBrowser
    public static final String MEDIA_ID_ROOT = "__ROOT__";
    public static final String MEDIA_ID_MUSICS_BY_ARTIST = "__BY_ARTIST__";
    public static final String MEDIA_ID_MUSICS_BY_ALBUM = "__BY_ALBUM__";
    public static final String MEDIA_ID_MUSICS_BY_SONG = "__BY_SONG__";
    public static final String MEDIA_ID_MUSICS_BY_PLAYLIST = "__BY_PLAYLIST__";
    public static final String MEDIA_ID_MUSICS_BY_SEARCH = "__BY_SEARCH__";
    public static final String MEDIA_ID_NOW_PLAYING = "__NOW_PLAYING__";
    public static final String MEDIA_ID_FOLDERS = "__FOLDERS__";

    private static final int MAX_ART_WIDTH = 800;  // pixels
    private static final int MAX_ART_HEIGHT = 480;  // pixels

    // Resolution reasonable for carrying around as an icon (generally in
    // MediaDescription.getIconBitmap). This should not be bigger than necessary, because
    // the MediaDescription object should be lightweight. If you set it too high and try to
    // serialize the MediaDescription, you may get FAILED BINDER TRANSACTION errors.
    private static final int MAX_ART_WIDTH_ICON = 128;  // pixels
    private static final int MAX_ART_HEIGHT_ICON = 128;  // pixels

    public List<MediaBrowser.MediaItem> collect(String parentMediaId) {
        try {
            switch (parentMediaId) {
                case MEDIA_ID_ROOT:
                    return collectRootMediaItems();
                case MEDIA_ID_FOLDERS:
                    return collectFoldersMediaItems();
                case MEDIA_ID_MUSICS_BY_PLAYLIST:
                    return collectPlaylistsMediaItems();
                default:
                    return new ArrayList<>();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }

    private List<MediaBrowser.MediaItem> collectRootMediaItems() {

        List<MediaBrowser.MediaItem> mediaItems = new ArrayList<>();

        List<MediaItemOwn> list = Arrays.asList(
                new MediaItemOwn(MEDIA_ID_FOLDERS, "Folders", false),
                new MediaItemOwn(MEDIA_ID_MUSICS_BY_PLAYLIST, "Playlists", false)
        );

        for (MediaItemOwn item : list) {
            mediaItems.add(new MediaBrowser.MediaItem(new MediaDescription.Builder().setMediaId(item.mediaId).setTitle(item.title).build(),
                    item.playable ? MediaBrowser.MediaItem.FLAG_PLAYABLE : MediaBrowser.MediaItem.FLAG_BROWSABLE));
        }

        return mediaItems;

    }

    private class MediaItemOwn {
        private String mediaId;
        private String title;
        private boolean playable;

        public MediaItemOwn(String mediaId, String title, boolean playable) {
            this.mediaId = mediaId;
            this.title = title;
            this.playable = playable;
        }
    }

    private List<MediaBrowser.MediaItem> collectPlaylistsMediaItems() {

        List<MediaBrowser.MediaItem> mediaItems = new ArrayList<>();

        Cursor c = service.getContentResolver().query(PowerampAPI.ROOT_URI.buildUpon().appendEncodedPath("playlists").build(),
                new String[]{
                        TableDefs.Playlists._ID,
                        TableDefs.Playlists.NAME,
                        TableDefs.Playlists.NUM_FILES_COUNT,
                },
                null, null, TableDefs.Playlists.NAME + " COLLATE NOCASE");

        if (c.moveToFirst()) {
            do {
                long id = c.getLong(0);
                String playlistName = c.getString(1);
                Long numFiles = c.getLong(2);

                MediaDescription desc =
                        new MediaDescription.Builder()
                                .setMediaId(MEDIA_ID_MUSICS_BY_PLAYLIST + "|" + String.valueOf(id))
                                .setTitle(playlistName)
                                .setSubtitle(numFiles + " Songs")
                                .build();

                MediaBrowser.MediaItem songList = new MediaBrowser.MediaItem(desc, MediaBrowser.MediaItem.FLAG_PLAYABLE);
                mediaItems.add(songList);
            } while (c.moveToNext());
        }

        c.close();

        return mediaItems;
    }

    private List<MediaBrowser.MediaItem> collectFoldersMediaItems() throws IOException {

        List<MediaBrowser.MediaItem> mediaItems = new ArrayList<>();

        Cursor c = service.getContentResolver().query(PowerampAPI.ROOT_URI.buildUpon().appendEncodedPath("folders").build(),
                new String[]{
                        TableDefs.Folders._ID,
                        TableDefs.Folders.NAME,
                        TableDefs.Folders.PARENT_NAME,
                        TableDefs.Folders.NUM_FILES,
                        TableDefs.Folders.THUMB,
                        TableDefs.Folders.PATH
                }, null, null, TableDefs.Folders.NAME + " COLLATE NOCASE");

        if (c.moveToFirst()) {
            do {
                DatabaseUtils.dumpCursor(c);
                long id = c.getLong(0);
                String folderName = c.getString(1);
                String rootFolder = c.getString(2);
                String numFiles = c.getString(3);
                String thumb = c.getString(4);
                String path = c.getString(5);

                Bitmap icon = null;

                //TODO
                /*if (thumb != null) {
                    String url = PathUtilities.resolveRelativePath(thumb, path);
                    Bitmap bitmap = BitmapHelper.fetchAndRescaleBitmap(url.startsWith("/") ? "file://" + url : url, MAX_ART_WIDTH, MAX_ART_HEIGHT);
                    icon = BitmapHelper.scaleBitmap(bitmap, MAX_ART_WIDTH_ICON, MAX_ART_HEIGHT_ICON);
                }*/

                MediaDescription.Builder builder = new MediaDescription.Builder()
                        .setMediaId(MEDIA_ID_FOLDERS + "|" + String.valueOf(id))
                        .setTitle(folderName)
                        .setSubtitle(numFiles + " Songs • " + (TextUtils.isEmpty(rootFolder) ? "/" : rootFolder));
                if (icon != null) builder.setIconBitmap(icon);

                MediaBrowser.MediaItem songList = new MediaBrowser.MediaItem(builder.build(), MediaBrowser.MediaItem.FLAG_PLAYABLE);
                mediaItems.add(songList);

            } while (c.moveToNext());
        }

        c.close();

        return mediaItems;
    }


}
