package net.patzleiner.powerampcar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadata;
import android.media.browse.MediaBrowser.MediaItem;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.service.media.MediaBrowserService;
import android.text.TextUtils;
import android.util.Log;

import com.maxmpz.poweramp.player.PowerampAPI;
import com.maxmpz.poweramp.player.TableDefs;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides a MediaBrowser through a service. It exposes the media library to a browsing
 * client, through the onGetRoot and onLoadChildren methods. It also creates a MediaSession and
 * exposes it through its MediaSession.Token, which allows the client to create a MediaController
 * that connects to and send control commands to the MediaSession remotely. This is useful for
 * user interfaces that need to interact with your media session, like Android Auto. You can
 * (should) also use the same service from your app's UI, which gives a seamless playback
 * experience to the user.
 * <p>
 * To implement a MediaBrowserService, you need to:
 * <p>
 * <ul>
 * <p>
 * <li> Extend {@link android.service.media.MediaBrowserService}, implementing the media browsing
 * related methods {@link android.service.media.MediaBrowserService#onGetRoot} and
 * {@link android.service.media.MediaBrowserService#onLoadChildren};
 * <li> In onCreate, start a new {@link android.media.session.MediaSession} and notify its parent
 * with the session's token {@link android.service.media.MediaBrowserService#setSessionToken};
 * <p>
 * <li> Set a callback on the
 * {@link android.media.session.MediaSession#setCallback(android.media.session.MediaSession.Callback)}.
 * The callback will receive all the user's actions, like play, pause, etc;
 * <p>
 * <li> Handle all the actual music playing using any method your app prefers (for example,
 * {@link android.media.MediaPlayer})
 * <p>
 * <li> Update playbackState, "now playing" metadata and queue, using MediaSession proper methods
 * {@link android.media.session.MediaSession#setPlaybackState(android.media.session.PlaybackState)}
 * {@link android.media.session.MediaSession#setMetadata(android.media.MediaMetadata)} and
 * {@link android.media.session.MediaSession#setQueue(java.util.List)})
 * <p>
 * <li> Declare and export the service in AndroidManifest with an intent receiver for the action
 * android.media.browse.MediaBrowserService
 * <p>
 * </ul>
 * <p>
 * To make your app compatible with Android Auto, you also need to:
 * <p>
 * <ul>
 * <p>
 * <li> Declare a meta-data tag in AndroidManifest.xml linking to a xml resource
 * with a &lt;automotiveApp&gt; root element. For a media app, this must include
 * an &lt;uses name="media"/&gt; element as a child.
 * For example, in AndroidManifest.xml:
 * &lt;meta-data android:name="com.google.android.gms.car.application"
 * android:resource="@xml/automotive_app_desc"/&gt;
 * And in res/values/automotive_app_desc.xml:
 * &lt;automotiveApp&gt;
 * &lt;uses name="media"/&gt;
 * &lt;/automotiveApp&gt;
 * <p>
 * </ul>
 *
 * @see <a href="README.md">README.md</a> for more details.
 */
public class PowerampService extends MediaBrowserService {

    //TODO PlaybackModes
    //TODO AlbumArts in mediaItems
    //TODO Notification if track changed??
    //TODO More MediaItems
    //TODO Logo
    //TODO playbackSpeed get from poweramp and set in playbackstate (No API method currently)
    //TODO Show settings in normal app
    //TODO settings if playlist starts from beginning or continues from last position

    //TODO enable always process albumart
    //TODO Voice search

    private static final String TAG = "PowerampService";
    private String MAXMPPACKAGE = "com.maxmpz.audioplayer";

    private MediaSession mSession;

    private Intent mTrackIntent;
    private Intent mTrackPosIntent;
    private Intent mAAIntent;
    private Intent mStatusIntent;

    private Bundle mCurrentTrack;
    private boolean paused = true;
    private MediaMetadata track = new MediaMetadata.Builder().build();
    private PlaybackState playbackState = new PlaybackState.Builder().build();
    private MediaItemsCollector mediaItemsCollector = new MediaItemsCollector(this);

    @Override
    public void onCreate() {
        super.onCreate();

        mSession = new MediaSession(this, "PowerampService");
        setSessionToken(mSession.getSessionToken());
        mSession.setCallback(new MediaSessionCallback());
        mSession.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);


        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player
        PlaybackState.Builder stateBuilder = new PlaybackState.Builder().setActions(
                PlaybackState.ACTION_PLAY);
        this.playbackState = stateBuilder.build();
        mSession.setPlaybackState(this.playbackState);


        // Note, it's not necessary to set mStatusIntent/mPlayingModeIntent this way here,
        // but this approach can be used with null receiver to get current sticky intent without broadcast receiver.
        mAAIntent = registerReceiver(mAAReceiver, new IntentFilter(PowerampAPI.ACTION_AA_CHANGED));
        mTrackIntent = registerReceiver(mTrackReceiver, new IntentFilter(PowerampAPI.ACTION_TRACK_CHANGED));
        mStatusIntent = registerReceiver(mStatusReceiver, new IntentFilter(PowerampAPI.ACTION_STATUS_CHANGED));
        mTrackPosIntent = registerReceiver(mTrackPosReceiver, new IntentFilter(PowerampAPI.ACTION_TRACK_POS_SYNC));
        //TODO mPlayingModeIntent = registerReceiver(mPlayingModeReceiver, new IntentFilter(PowerampAPI.ACTION_PLAYING_MODE_CHANGED));

        startService(PowerampAPI.newAPIIntent().putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.POS_SYNC));

        updateTrackUI();
        updatePlaybackState(null);
        updateAlbumArt();
        processTrackIntent();
    }

    @Override
    public void onDestroy() {
        startService(new Intent(PowerampAPI.ACTION_API_COMMAND).putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.PAUSE).setPackage(MAXMPPACKAGE));
        mSession.release();
        unregister();
    }

    private void unregister() {
        if (mTrackIntent != null) {
            try {
                unregisterReceiver(mTrackReceiver);
            } catch (Exception ex) {
            } // Can throw exception if for some reason broadcast receiver wasn't registered.
        }
        if (mTrackPosIntent != null) {
            try {
                unregisterReceiver(mTrackPosReceiver);
            } catch (Exception ex) {
            }
        }
        if (mAAIntent != null) {
            try {
                unregisterReceiver(mAAReceiver);
            } catch (Exception ex) {
            }
        }
        if (mStatusIntent != null) {
            try {
                unregisterReceiver(mStatusReceiver);
            } catch (Exception ex) {
            }
        }
        /*if(mPlayingModeReceiver != null) {
            try {
                unregisterReceiver(mPlayingModeReceiver);
            } catch(Exception ex) {}
        }*/
    }

    @Override
    public BrowserRoot onGetRoot(String clientPackageName, int clientUid, Bundle rootHints) {
        return new BrowserRoot(MediaItemsCollector.MEDIA_ID_ROOT, null);
    }

    @Override
    public void onLoadChildren(final String parentMediaId, final Result<List<MediaItem>> result) {

        List<MediaItem> mediaItems = new ArrayList<>();

        mediaItems.addAll(mediaItemsCollector.collect(parentMediaId));

        result.sendResult(mediaItems);
    }

    private final class MediaSessionCallback extends MediaSession.Callback {
        @Override
        public void onPlay() {
            startService(new Intent(PowerampAPI.ACTION_API_COMMAND).putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.RESUME).setPackage(MAXMPPACKAGE));
        }

        @Override
        public void onSkipToQueueItem(long queueId) {
        }

        @Override
        public void onSeekTo(long position) {
            startService(new Intent(PowerampAPI.ACTION_API_COMMAND).putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.SEEK).putExtra(PowerampAPI.Track.POSITION, position / 1000).setPackage(MAXMPPACKAGE));
        }

        @Override
        public void onPlayFromMediaId(String mediaId, Bundle extras) {
            String[] mediaIds = mediaId.split("\\|");
            switch (mediaIds[0]) {
                case MediaItemsCollector.MEDIA_ID_FOLDERS:
                    Cursor c = getContentResolver().query(PowerampAPI.ROOT_URI.buildUpon().appendEncodedPath("folders").appendEncodedPath(mediaIds[1]).appendEncodedPath("files").build(),
                            new String[]{TableDefs.Files._ID},
                            null, null, TableDefs.Files.NAME + " COLLATE NOCASE");
                    if (c != null) {
                        if (c.moveToNext()) {
                            startService(new Intent(PowerampAPI.ACTION_API_COMMAND)
                                    .putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.OPEN_TO_PLAY)
                                    .setPackage(MAXMPPACKAGE)
                                    .setData(PowerampAPI.ROOT_URI.buildUpon()
                                            .appendEncodedPath("folders")
                                            .appendEncodedPath(mediaIds[1])
                                            .appendEncodedPath("files")
                                            .appendEncodedPath(c.getString(0))
                                            .build()));
                        }
                        c.close();
                    }
                    break;
                case MediaItemsCollector.MEDIA_ID_MUSICS_BY_PLAYLIST:
                    String firstTrackId = null;
                    /*//TODO if (settingsPlayFirstItem && !shuffle) {
                        Cursor c2 = getContentResolver().query(PowerampAPI.ROOT_URI.buildUpon().appendEncodedPath("playlists").appendEncodedPath(mediaIds[1]).appendEncodedPath("files").build(),
                                new String[]{TableDefs.PlaylistEntries._ID},
                                null, null, TableDefs.PlaylistEntries._ID + " COLLATE NOCASE"); //We have to play the first song else it continues with the last played song in this playlist
                        //TODO If shuffle is on, pick a random song
                        if (c2 != null) {
                            if (c2.moveToNext()) {
                                firstTrackId = c2.getString(0);
                            }
                            c2.close();
                        }
                    //}*/
                    Uri.Builder builder = PowerampAPI.ROOT_URI.buildUpon()
                            .appendEncodedPath("playlists")
                            .appendEncodedPath(mediaIds[1])
                            .appendEncodedPath("files");
                    if (firstTrackId != null) {
                        builder.appendEncodedPath(firstTrackId);
                    }
                    startService(new Intent(PowerampAPI.ACTION_API_COMMAND)
                            .putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.OPEN_TO_PLAY)
                            .setPackage(MAXMPPACKAGE)
                            .setData(builder.build()));

                    break;
            }
        }

        @Override
        public void onPause() {
            startService(new Intent(PowerampAPI.ACTION_API_COMMAND).putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.PAUSE).setPackage(MAXMPPACKAGE));
        }

        @Override
        public void onStop() {
            startService(new Intent(PowerampAPI.ACTION_API_COMMAND).putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.STOP).setPackage(MAXMPPACKAGE));
        }

        @Override
        public void onSkipToNext() {
            startService(new Intent(PowerampAPI.ACTION_API_COMMAND).putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.NEXT).setPackage(MAXMPPACKAGE));
        }

        @Override
        public void onSkipToPrevious() {
            startService(new Intent(PowerampAPI.ACTION_API_COMMAND).putExtra(PowerampAPI.COMMAND, PowerampAPI.Commands.PREVIOUS).setPackage(MAXMPPACKAGE));
        }

        @Override
        public void onCustomAction(String action, Bundle extras) {
        }

        @Override
        public void onPlayFromSearch(final String query, final Bundle extras) {
        }
    }


    private BroadcastReceiver mTrackReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mTrackIntent = intent;
            processTrackIntent();
            Log.w(TAG, "mTrackReceiver " + intent);
        }
    };

    private BroadcastReceiver mTrackPosReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mTrackPosIntent = intent;
            updateTrackPos();
            Log.w(TAG, "mTrackPosReceiver " + intent);
        }
    };

    private BroadcastReceiver mAAReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mAAIntent = intent;

            updateAlbumArt();

            Log.w(TAG, "mAAReceiver " + intent);
        }
    };

    private BroadcastReceiver mStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mStatusIntent = intent;
            updatePlaybackState(null);
            Log.w(TAG, "mStatusReceiver " + intent);
        }
    };

    private void processTrackIntent() {
        mCurrentTrack = null;

        if (mTrackIntent != null) {
            mCurrentTrack = mTrackIntent.getBundleExtra(PowerampAPI.TRACK);
            updateTrackUI();
        }
    }

    // This method updates track related info.
    private void updateTrackUI() {
        Log.w(TAG, "updateTrackUI");

        if (mTrackIntent != null) {
            if (mCurrentTrack != null) {

                track = new MediaMetadata.Builder()
                        .putString(MediaMetadata.METADATA_KEY_TITLE, mCurrentTrack.getString(PowerampAPI.Track.TITLE))
                        .putString(MediaMetadata.METADATA_KEY_ARTIST, mCurrentTrack.getString(PowerampAPI.Track.ARTIST))
                        .putString(MediaMetadata.METADATA_KEY_ALBUM, mCurrentTrack.getString(PowerampAPI.Track.ALBUM))
                        .putLong(MediaMetadata.METADATA_KEY_DURATION, mCurrentTrack.getInt(PowerampAPI.Track.DURATION) * 1000).build();

                mSession.setMetadata(track);

                //Done by TrackPosSync mSession.setPlaybackState(new PlaybackState.Builder(playbackState).setState(playbackState.getState(), 0, playbackState.getPlaybackSpeed(), SystemClock.elapsedRealtime()).build());

                return;
            }
        }
        // Else clean everything.
        mSession.setMetadata(null);
    }

    private void updateTrackPos() {
        this.playbackState = new PlaybackState.Builder(playbackState).setState(playbackState.getState(),
                mTrackPosIntent.getIntExtra(PowerampAPI.Track.POSITION, -1) * 1000,
                playbackState.getPlaybackSpeed(), SystemClock.elapsedRealtime()).build();
        mSession.setPlaybackState(this.playbackState);
    }

    private void updateAlbumArt() {
        Log.w(TAG, "updateAlbumArt");
        String directAAPath = mAAIntent.getStringExtra(PowerampAPI.ALBUM_ART_PATH);

        if (!TextUtils.isEmpty(directAAPath)) {
            Log.w(TAG, "has AA, albumArtPath=" + directAAPath);

            track = new MediaMetadata.Builder(track)
                    .putBitmap(MediaMetadata.METADATA_KEY_ART, null).putString(MediaMetadata.METADATA_KEY_ALBUM_ART_URI, directAAPath).build();

            mSession.setMetadata(track);

        } else if (mAAIntent.hasExtra(PowerampAPI.ALBUM_ART_BITMAP)) {

            Bitmap albumArtBitmap = mAAIntent.getParcelableExtra(PowerampAPI.ALBUM_ART_BITMAP);
            if (albumArtBitmap != null) {
                Log.w(TAG, "has AA, bitmap");
                track = new MediaMetadata.Builder(track)
                        .putBitmap(MediaMetadata.METADATA_KEY_ART, albumArtBitmap).putString(MediaMetadata.METADATA_KEY_ALBUM_ART_URI, null).build();

                mSession.setMetadata(track);
            }
        } else {
            Log.w(TAG, "no AA");
            track = new MediaMetadata.Builder(track)
                    .putBitmap(MediaMetadata.METADATA_KEY_ART, null).putString(MediaMetadata.METADATA_KEY_ALBUM_ART_URI, null).build();

            mSession.setMetadata(track);
        }
    }

    /**
     * Update the current media player state, optionally showing an error message.
     *
     * @param error if not null, error message to present to the user.
     */
    private void updatePlaybackState(String error) {
        Log.d(TAG, "updatePlaybackState");

        int status = mStatusIntent.getIntExtra(PowerampAPI.STATUS, -1);
        paused = mStatusIntent.getBooleanExtra(PowerampAPI.PAUSED, false);
        // Each status update can contain track position update as well.
        int pos = mStatusIntent.getIntExtra(PowerampAPI.Track.POSITION, -1) * 1000;

        PlaybackState.Builder stateBuilder =
                new PlaybackState.Builder().setActions(getAvailableActions(paused));

        int state = PlaybackState.STATE_NONE; //STATE_NONE not showing current track

        switch (status) {
            case PowerampAPI.Status.TRACK_PLAYING:
                state = PlaybackState.STATE_PLAYING;
                if (paused) state = PlaybackState.STATE_PAUSED;
                break;

            case PowerampAPI.Status.TRACK_ENDED:
            case PowerampAPI.Status.PLAYING_ENDED:
                state = PlaybackState.STATE_PAUSED;
                break;
        }

        // If there is an error message, send it to the playback state:
        if (error != null) {
            // Error states are really only supposed to be used for errors that cause playback to
            // stop unexpectedly and persist until the user takes action to fix it.
            stateBuilder.setErrorMessage(error);
            state = PlaybackState.STATE_ERROR;
        }

        long position = pos;

        if (pos == -1000) {
            position = playbackState.getPosition();
        } else if (pos < 0) {
            position = PlaybackState.PLAYBACK_POSITION_UNKNOWN;
        }

        stateBuilder.setState(state, position, 1.0f, SystemClock.elapsedRealtime());
        this.playbackState = stateBuilder.build();
        mSession.setPlaybackState(playbackState);
    }

    private long getAvailableActions(boolean paused) {
        long actions = PlaybackState.ACTION_PLAY | PlaybackState.ACTION_PLAY_FROM_MEDIA_ID
                | PlaybackState.ACTION_PLAY_FROM_SEARCH | PlaybackState.ACTION_PLAY_PAUSE;
        if (!paused) {
            actions |= PlaybackState.ACTION_PAUSE;
        }
        actions |= PlaybackState.ACTION_SKIP_TO_PREVIOUS | PlaybackState.ACTION_SKIP_TO_NEXT;
        return actions;
    }

}
