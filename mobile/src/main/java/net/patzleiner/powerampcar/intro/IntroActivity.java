package net.patzleiner.powerampcar.intro;

import android.Manifest;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import net.patzleiner.powerampcar.R;

/**
 * @author Gabriel Patzleiner
 */
public class IntroActivity extends AppIntro {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Note here that we DO NOT use setContentView();

        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
        addSlide(AppIntroFragment.newInstance("Welcome", "PowerAmpCar let's you control PowerAmp on your car", R.drawable.ic_info_black_24dp, Color.parseColor("#9C27B0")));
        addSlide(AppIntroFragment.newInstance("Permissions", "We need the READ_EXTERNAL_STORAGE permission in order to read album arts on your external storage", R.drawable.ic_info_black_24dp, Color.parseColor("#9C27B0")));

        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(false);
        setProgressButtonEnabled(true);

        // Ask for READ_EXTERNAL_STORAGE permission on the first slide
        askForPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2); // OR

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}